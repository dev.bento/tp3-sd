# TP3-SD

## Para fazer

- [x] Criar o servidor socket para lidar com as requisições
- [x] Fazer as funções rodarem em threads
- [x] Criar a interface grafica para o subscriber
- [x] Enviar as atualizações para o Topic
- [x] Integrar a interface no prompt com o servidor
- [x] Notificar as outras instâncias sobre os requests de atualização (update)
- [ ] Atualizar o canvas_id quando carregar os pontos
- [ ] Corrigir os bugs na notificação dos subscribers
- [ ] Integrar o publisher ao topic
- [ ] Conectar com outro Topic quando um cair
