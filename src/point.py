from random import random
import time

class Point():

	def __init__(self, coord_x, coord_y, color, border, radius, canvas_id = None):
		self.id = str(int(random()*10**10))
		self.color = color
		self.border = border
		self.coord_x = coord_x
		self.coord_y = coord_y
		self.radius = radius
		self.canvas_ids = {}
	
	def toJson(self):
		return { 
				"id": self.id,
				"color": self.color,
				"border": self.border,
				"coord_x": self.coord_x,
				"coord_y": self.coord_y,
				"radius": self.radius,
				"canvas_ids": self.canvas_ids
		}