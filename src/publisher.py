
import sys
import time
import math
import copy
import json
import zlib
import socket
import random
import asyncio
import argparse
import subprocess
# import _thread
from threading import Thread

from point import Point
from typefy import Types
from server import Server
from typefy import flags

BUFFER_SIZE = 100

class Publisher(Server):

    topics = {}
    sendTo = {}
    points = {}

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        self.colorId = '#%02x%02x%02x' % (r, g, b)

        self.id = f"{self.ip}:{self.port}"
        self.type = '(P)'
        
        if not self.name:
            self.name = 'Publisher'
        
        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as msg:
            raise
            # print("Socket creation error: ", str(msg))

        print (self.type)
        if (self.external_topic_ip and self.external_topic_port):
            self.requestTopicInstances(self.external_topic_ip, self.external_topic_port, self.type)

        super(Publisher, self).__init__(**kwargs)

    
    def run(self):
        while self.isRunning:
            try:
                time.sleep(1)
                self.createNPoints(random.randint(1, 3))
                print(self.name, json.dumps(list(self.points), indent = 2))
                time.sleep(.5)
                self.removeNRandomPointsByIds(random.randint(1, 5))
                print(self.name, json.dumps(list(self.points), indent = 2))
            except KeyboardInterrupt as e:
                break
            except Exception as e:
                break
                raise

    def insertPoints(self, points, isNotifiable=True):
        try:
            self.points.update(points)

            if isNotifiable:
                self.notify(self.sendTo, {
                    flags.INSERT_POINTS: {
                        'sent_by': self.parseSelfToJson(),
                        'points': points,
                    },
                })
                # pass
        except Exception as e:
            raise

    def createNPoints(self, n_of_points):
        # print("\n::::::: CREATING POINTS :::::::")
        points_to_add = {}
        for n in range(n_of_points):
            x = random.randint(25, 550)
            y = random.randint(25, 350)
            p = self.createPoint(x, y)
            points_to_add.update({p.id: p.toJson()})
            # print("->", p.id)
        self.insertPoints(points_to_add, isNotifiable = True)

    def insertReceivedPoints(self, points):
        # print("\n::::::: INSERTING RECEIVED POINTS :::::::")
        if not self.points:
            for key, value in points.items():
                try:                     
                    # print("->", point_id)
                    self.insertPoints({key: value}, isNotifiable = False)
                except:
                    continue
            return
            

    def removePointByIds(self, ids, isNotifiable=True):
        try:
            # print("\n::::::: REMOVING POINTS :::::::")
            for id in ids:
                if (self.points.get(id)):
                    point = self.points.get(id)
                    point_to_remove = self.points.pop(id)
                    # print('->', id)

            if isNotifiable:
                self.notify(self.sendTo, {
                    flags.REMOVE_POINTS: {
                        'sent_by': self.parseSelfToJson(),
                        'points': ids,
                    },
                })
                # pass
        except Exception as e:
            raise

    def removeNRandomPointsByIds(self, n_of_points):
        points_to_remove = []
        if self.points:
            for n in range(n_of_points):
                point = random.choice(list(self.points))
                if (point):
                    points_to_remove.append(point)
        self.removePointByIds(points_to_remove, isNotifiable = True)

    def updatePoint(self, point, isNotifiable=True, **kwargs):
        # print("\n::::::: UPDATING POINT :::::::")
        try:
            if point is None or kwargs is None:
                return

            # print ("-> id:", point.get('id'))
            for key, value in kwargs.items():
                # print(key, value)
                self.points[point.get('id')][key] = value

            if isNotifiable:
                self.notify(self.sendTo, {
                    'UPDATE_POINTS': {
                        'sent_by': self.parseSelfToJson(),
                        'points': { point.get('id'): point },
                    },
                })

        except Exception as e:
            raise

    # def movePoint(self, x, y, point):
    #     selfx = (coords[0] + coords[2])/2
    #     selfy = (coords[1] + coords[3])/2
    #     mousex, mousey = x, y
    #     directDist = math.sqrt(((mousex-selfx) ** 2) + ((mousey-selfy) ** 2))
    #     movex = (mousex-selfx)
    #     movey = (mousey-selfy)
    #     self.updatePoint(point, coord_x=mousex, coord_y=mousey, isNotifiable=True)

    def createPoint(self, x, y):
        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        color = '#%02x%02x%02x' % (r, g, b)
        radius = random.randint(5, 25)
        p = Point(coord_x=x, coord_y=y, color=color, radius=radius, border= self.colorId)
        return p

    def changePointColor(self, point):
        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        color = '#%02x%02x%02x' % (r, g, b)
        self.updatePoint(point, color=color, isNotifiable=True)

    def handleRequest(self, request):

        try:
            request_flag = list(request.keys())[0]
        except:
            pass
        
        # If the request was send by me, return
        if request.get(request_flag).get('sent_by').get('id') is self.id:
            return "SUCCESS"

        # print("\n::::::: REQUEST FLAG :::::::")

        if request_flag == flags.UPDATE_POINTS:
            print('UPDATE_POINTS')
            points_to_add = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')
            print("-> BY:", sent_by.get('id'))
            
            # print("\n::::::: UPDATING POINTS :::::::")
            for point in points_to_add:
                self.updatePoint(point, isNotifiable=False)

            return "SUCCESS"

        if request_flag == flags.INSERT_POINTS:
            print('INSERT_POINTS')
            points_to_add = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')

            # print("\n::::::: INSERTING POINTS :::::::")
            print("-> BY:", sent_by.get('id'))

            self.insertPoints(points_to_add, isNotifiable=False)
            
            return "SUCCESS"

        if request_flag == flags.REMOVE_POINTS:
            print('REMOVE_POINTS')
            
            points_to_remove = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')
            print("-> BY:", sent_by.get('id'))
            
            # print("\n::::::: REMOVING POINTS :::::::")
            self.removePointByIds(points_to_remove, isNotifiable=False)
            return "SUCCESS"
            
        if request_flag == flags.TOPIC_INSTANCES_RESPONSE:
            # print('TOPIC_INSTANCES_RESPONSE')
            received_topics = request.get(request_flag).get('topics')
            received_points = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')

            # Registre the sent_by topic
            self.registerTopic(sent_by)

            # Set the main topic to connect
            self.sendTo = list(sent_by.values())[0]
            # print("-> (CONNECTED TO)", self.sendTo.get('id'))

            # Remove known topics from request
            for key in list(self.topics):
                if received_topics.get(key):
                    del received_topics[key]

            # Register the new topics
            self.registerTopic(received_topics)           

            # Save reveived points
            if received_points:
                self.insertReceivedPoints(received_points)

            return "SUCCESS"
        

    def registerTopic(self, topics=None):
        if topics:
            self.topics.update(topics)
            # print("\n::::::: REGISTRED TOPICS :::::::")
            # print(json.dumps(list(self.topics), indent=2))
        # pass


parser = argparse.ArgumentParser(description='Creating a Topic instance')

parser.add_argument('-tip', '--external_topic_ip', help='IP of another Topic Instance')
parser.add_argument('-tp', '--external_topic_port',
                    help='PORT of nother Topic Instance')
parser.add_argument('-ip', '--ip', help='IP of this Topic Instance')
parser.add_argument('-p', '--port', help='PORT of this Topic Instance')
parser.add_argument('-n', '--name', help='This Topic Instance name')
# parser.add_argument('-t', '--time_interval', help='The sleep time between each update')

args = parser.parse_args()

args = vars(args)
try:
    pub = Publisher(**args)
    publisher_server = Thread(target=pub.listen)
    publisher_server.daemon = False
    publisher_server.start()
    pub.run()
except Exception as e:
        raise
