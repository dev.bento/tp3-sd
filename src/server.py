import sys
import time
import math
import copy
import json
import socket
import random
from threading import Thread
from queue import Queue

from point import Point
from typefy import Types
from typefy import flags
from interface import showLog

BUFFER_SIZE = 100

class Server():
    isRunning = True
    topics = {}
    updates_to_notify = Queue()

    def __init__(self, ip, port, name, **kwargs):
        self.__dict__.update(kwargs)

        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as msg:
            raise
            # print("Socket creation error: ", str(msg))

    def parseSelfToJson(self):
        return {
                'id': self.id,
                'ip': self.ip,
                'port': self.port,
                'name': self.name,
                'type': self.type,
            }

    def listen(self):
        self.sock.bind((self.ip, int(self.port)))
        # print(f"\n::::::: {self.name.upper()} BINDED TO {self.ip}:{self.port} :::::::")
        
        self.sock.listen(5)

        # notifier = Thread(target=self.notifyAll)
        # notifier.daemon = False
        # notifier.start()        
        # showLog(
        #         title= self.name.upper(),
        #         ip=self.ip, 
        #         port=self.port, 
        #         connect_to=list(self.topics),
        #         registred={},
        #         request_flag= '',
        #         request_data= [], 
        #         list_points = list(self.points)
        #     )
                        
        while True:
            # print(f"\n::::::: {self.name.upper()} LISTENING AT PORT {self.port} :::::::")
            try:
                conn, addr = self.sock.accept()
                receiver = Thread(target=self.handleConnection,args=[conn, addr])
                receiver.daemon = False
                receiver.start()
                # receiver.join()
            except Exception as e:
                raise
            except KeyboardInterrupt as e:
                self.isRunning = False
                # self.closeAllConnections()
                # print(f"\n::::::: SHUTTING TOPIC SERVER ({self.id}) DOWN :::::::")
                break
    
    def handleConnection(self, conn, addr):
        request_size = int(conn.recv(BUFFER_SIZE).decode('utf-8'))
        conn.send(b'READY')
        request = b''
        data = b''
        while request_size > 0:
            data = conn.recv(BUFFER_SIZE)
            request += data
            request_size -= len(data)
        request = json.loads(request.decode('utf-8'))
        # print(f"\n::::::: RECEIVED FROM {addr} :::::::")
        # print(json.dumps(request))
        response = self.handleRequest(request)
        conn.send(str(len(response)).encode('utf-8'))
        conn.recv(BUFFER_SIZE).decode('utf-8') #READY confirmation
        conn.send(response.encode('utf-8'))
        conn.close()
        # print(self.name, json.dumps(list(self.points), indent = 2))

    def handleRequest(self, request):
        response = request
        return response

    def notify(self, instance, message):
        if self.isRunning:
            sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            try:
                # print(f"\n-> {instance.get('id')}")
                sock.connect_ex((instance.get('ip'), int(instance.get('port'))))
                message = json.dumps(message).encode('utf-8')
                sock.send(str(len(message)).encode('utf-8'))
                sock.recv(BUFFER_SIZE) #READY confirmation
                sock.send(message)
                response = sock.recv(BUFFER_SIZE).decode('utf-8')
            except BrokenPipeError as e:
                # buscar o proximo topic na lista e chamar a notificao novamente
                return False
                raise
            except Exception as e:
                raise
            sock.close()
            return True
    
    def notifyTopics(self, topics, message):
        # print(f"\n::::::: NOTIFYING :::::::")
        for key, topic in topics.items():
            notifier = Thread(target=self.notify, args=[topic, message])
            notifier.daemon = False
            notifier.start()        

    def requestTopicInstances(self, ip, port, request_type):
        # print("\n::::::: REQUESTING TOPIC INSTANCE :::::::")
        
        print(ip, port)
        if ip is None and port is None:
            return

        message = json.dumps({
                flags.REGISTER_AND_REQUEST_TOPIC_INSTANCES: {
                    'request_by': self.parseSelfToJson(),
                    'type': request_type,
                }
            }).encode('utf-8')
        
        server_addr = (ip, int(port))
        
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # print(f"\n::::::: REQUESTING INSTANCES TO {server_addr} :::::::")
            sock.connect_ex(server_addr)
            try:
                sock.send(str(len(message)).encode('utf-8'))
            except Exception:
                sock.close()
                print(f"Conexão Falhou")
                return
            sock.recv(BUFFER_SIZE) #READY confirmation
            sock.send(message)
            response_size = int(sock.recv(BUFFER_SIZE).decode('utf-8'))
            sock.send(b'READY')
            response = b''
            data = b''
            while response_size > 0:
                data = sock.recv(BUFFER_SIZE)
                response += data
                response_size -= len(data)
            response = json.loads(response.decode('utf-8'))
            
            self.handleRequest(response)

        except Exception as e:
            raise
        sock.close()