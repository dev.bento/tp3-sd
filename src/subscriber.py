
import sys
import time
import math
import copy
import json
import zlib
import socket
import random
import asyncio
import argparse
import subprocess
from threading import Thread
from tkinter import *
from tkinter import ttk

from point import Point
from server import Server
from typefy import Types
from interface import showLog
from typefy import flags



class Subscriber(Server):
    '''
    Create new point: Left Click
    Remove point: Right Click

    '''

    points = {}
    topics = {}
    sendTo = {}
    window = Tk()

    def __init__(self, width=600, height=400, **kwargs):
        self.__dict__.update(kwargs)
        self.width = width
        self.height = height
        self.type = '(S)'

        self.id = f"{self.ip}:{self.port} {self.type}"

        if not self.name:
            self.name = 'Subcriber'

        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        self.colorId = '#%02x%02x%02x' % (r, g, b)

        self.window.geometry("{}x{}".format(width, height+30))
        # self.window.resizable(0, 0)
        self.canva = Canvas(self.window, width=width,
                            height=height, bg="white",)
        self.canva.bind("<Button-1>", self.leftclick)
        self.canva.bind("<Button-2>", self.midleclick)
        self.canva.bind("<Button-3>", self.rightclick)
        # self.canva.bind("<Configure>", self.on_resize)
        self.canva.pack(expand=True, fill=BOTH)
        # self.canva.pack()
        self.footer = Frame(master=self.window)
        self.footer.pack(fill=BOTH)

        label1 = Label(master=self.footer, text='Inserir Pontos: ', font=(
            'helvetica', 12)).grid(row=1, column=1, padx=5)
        input_text = IntVar()
        Entry(master=self.footer, font=('Verdana', 13), width="5",
              textvariable=input_text).grid(row=1, column=2, padx=5)
        ttk.Button(master=self.footer, text='Inserir', command=lambda nP=input_text: self.generateNPoints(
            nP)).grid(row=1, column=3, padx=5)

        label2 = Label(master=self.footer, text='Remover Pontos: ', font=(
            'helvetica', 12)).grid(row=1, column=10, padx=5)
        input_text2 = IntVar()
        Entry(master=self.footer, font=('Verdana', 13), width="5",
              textvariable=input_text2).grid(row=1, column=11, padx=5)
        ttk.Button(master=self.footer, text='Remover', command=lambda nP=input_text2:  self.removeNRandomPointsByIds(
            nP)).grid(row=1, column=12, padx=5)

        if (self.external_topic_ip and self.external_topic_port):
            self.requestTopicInstances(
                self.external_topic_ip, self.external_topic_port, self.type)

        showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=self.sendTo.get('id'),
                registred=list(self.topics),
                request_flag='',
                request_data={},
                list_points=list(self.points)
            )
        super(Subscriber, self).__init__(**kwargs)

    def run(self):
        self.window.mainloop()

    # def on_resize(self,event):
    # 	# determine the ratio of old width/height to new width/height
    # 	wscale = float(event.width)/self.width
    # 	hscale = float(event.height)/self.height
    # 	self.width = event.width
    # 	self.height = event.height
    # 	# resize the canvas
    # 	self.canva.config(width=self.width, height=self.height)
    # 	# rescale all the objects tagged with the "all" tag
    # 	self.canva.scale("all",0,0,wscale,hscale)

    def leftclick(self, event):
        x0 = event.x
        y0 = event.y

        # look for closest point
        try:
            item = self.canva.find_closest(x0, y0)
            tag = self.canva.gettags(item)
            p = None
            if 'current' in tag:
                p = self.points.get(tag[0])
            else:
                # print("\n::::::: INSERTIN POINT :::::::")
                p = self.createPoint(x0, y0)
                self.insertPoints({p.id: p.toJson()}, isNotifiable=True)
                # print("->", p.id)
                p = p.toJson()
            self.canva.bind(
                "<B1-Motion>", lambda e=event: self.movePoint(e, item=item, point=p))

        except Exception as e:
            raise

        self.canva.bind("<Button-1>", self.leftclick)

    def rightclick(self, event):
        x0 = event.x
        y0 = event.y

        # look for closest point
        try:
            item = self.canva.find_closest(x0, y0)
            tag = self.canva.gettags(item)
            if 'current' in tag:
                self.removePointByIds(ids=[tag[0]], isNotifiable=True)
        except Exception as e:
            raise

        self.canva.bind("<Button-3>", self.rightclick)

    def midleclick(self, event):
        x0 = event.x
        y0 = event.y

        # look for closest point
        try:
            item = self.canva.find_closest(x0, y0)
            tag = self.canva.gettags(item)
            if 'current' in tag:
                p = self.points.get(tag[0])
                self.changePointColor(point=p)
        except Exception as e:
            raise

        self.canva.bind("<Button-2>", self.midleclick)

    def insertPoints(self, points, isNotifiable=True):
        try:
            self.points.update(points)
            if isNotifiable:
                self.notify(self.sendTo, {
                    flags.INSERT_POINTS: {
                        'sent_by': self.parseSelfToJson(),
                        'points': points,
                        'isNotifiable': True,
                    },
                })
            showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=self.sendTo.get('id'),
                registred=list(self.topics),
                request_flag='INSERT_POINTS',
                request_data=points,
                list_points=list(self.points)
            )
        except Exception as e:
            raise

    def generateNPoints(self, n_of_points):
        # print("\n::::::: GERTAING POINTS :::::::")
        # print(n_of_points.get())
        points_to_add = {}
        for n in range(n_of_points.get()):
            x = random.randint(25, self.width-25)
            y = random.randint(25, self.height-25)
            p = self.createPoint(x, y)
            points_to_add.update({p.id: p.toJson()})
            # print("->", p.id)
        self.insertPoints(points_to_add, isNotifiable=True)

    def insertReceivedPoints(self, points):
        # print("\n::::::: INSERTING RECEIVED POINTS :::::::")
        if self.points:
            for point_id, point in points.items():
                if point and point.get('canvas_ids').get(self.id):
                    self.updatePoint(points.get(point_id), isNotifiable=False)
                    continue
                canvas_id = self.drawPoint(
                    id=point.get('id'),
                    coord_x=point.get('coord_x'),
                    coord_y=point.get('coord_y'),
                    color=point.get('color'),
                    radius=point.get('radius'),
                    border=point.get('border')
                )
                point.get('canvas_ids')[self.id] = canvas_id
                # print("->", point_id)
                # print (point)
                self.insertPoints(dict({point_id: point}), isNotifiable=False)
            return

        for point_id, point in points.items():
            canvas_id = self.drawPoint(
                id=point.get('id'),
                coord_x=point.get('coord_x'),
                coord_y=point.get('coord_y'),
                color=point.get('color'),
                radius=point.get('radius'),
                border=point.get('border')
            )
            point.get('canvas_ids')[self.id] = canvas_id
            self.insertPoints(dict({point_id: point}), isNotifiable=False)

    def removePointByIds(self, ids, isNotifiable=False):
        try:
            # print("\n::::::: REMOVING POINTS :::::::")
            for id in ids:
                point = self.points.get(id)
                if (point):
                    self.canva.delete(point.get('canvas_ids').get(self.id))
                    point_to_remove = self.points.pop(id)
                    # print('->', id)

            if isNotifiable:
                self.notify(self.sendTo, {
                    flags.REMOVE_POINTS: {
                        'sent_by': self.parseSelfToJson(),
                        'points': ids,
                        'isNotifiable': True,
                    },
                })

            showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=self.sendTo.get('id'),
                registred=list(self.topics),
                request_flag='REMOVE_POINTS',
                request_data=ids,
                list_points=list(self.points)
            )
        except Exception as e:
            raise

    def removeNRandomPointsByIds(self, n_of_points):
        points_to_remove = []
        for n in range(n_of_points.get()):
            x = random.randint(25, self.width-25)
            y = random.randint(25, self.height-25)
            point = self.canva.find_closest(x, y)
            # print(point)
            if (point):
                tag = self.canva.gettags(point[0])
                points_to_remove.append(tag[0])
        self.removePointByIds(points_to_remove, isNotifiable=True)

    def updatePoint(self, point, isNotifiable=True, **kwargs):
        # print("\n::::::: UPDATING POINT :::::::")
        try:
            if point is None or kwargs is None:
                return

            # print ("-> id:", point)
            for key, value in kwargs.items():
                # print(key, value)
                self.points[point.get('id')][key] = value

            if isNotifiable:
                self.notify(self.sendTo, {
                    flags.UPDATE_POINTS: {
                        'sent_by': self.parseSelfToJson(),
                        'points': dict({point.get('id'): point}),
                        'isNotifiable': True,
                    },
                })
            showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=self.sendTo.get('id'),
                registred=list(self.topics),
                request_flag='UPDATE_POINTS',
                request_data=point,
                list_points=list(self.points)
            )

        except Exception as e:
            raise

    def movePoint(self, event, item, point):
        coords = self.canva.coords(point.get('canvas_ids').get(self.id))
        selfx = (coords[0] + coords[2])/2
        selfy = (coords[1] + coords[3])/2
        mousex, mousey = event.x, event.y
        directDist = math.sqrt(((mousex-selfx) ** 2) + ((mousey-selfy) ** 2))
        movex = (mousex-selfx)
        movey = (mousey-selfy)
        self.updatePoint(point, coord_x=mousex,
                         coord_y=mousey, isNotifiable=True)
        self.canva.move(point.get('canvas_ids').get(self.id), movex, movey)

    def drawPoint(self, id, coord_x, coord_y, radius, color, border):
        x = coord_x
        y = coord_y
        r = radius
        c = color
        return self.canva.create_oval(x-r, y-r, x+r, y+r, fill=c, outline=border, width=4, tags=id)

    def createPoint(self, x, y):
        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        color = '#%02x%02x%02x' % (r, g, b)
        radius = random.randint(5, 25)
        p = Point(coord_x=x, coord_y=y, color=color,
                  radius=radius, border=self.colorId)
        canvas_id = self.drawPoint(
            id=p.id, coord_x=p.coord_x, coord_y=p.coord_y, color=p.color, radius=p.radius, border=self.colorId
        )
        p.canvas_ids[self.id] = canvas_id
        return p

    def changePointColor(self, point):
        r = random.randint(0, 255)
        g = random.randint(0, 255)
        b = random.randint(0, 255)
        color = '#%02x%02x%02x' % (r, g, b)
        self.canva.itemconfig(point.get('canvas_ids').get(self.id), fill=color)
        self.updatePoint(point, color=color, isNotifiable=True)

    def handleRequest(self, request):

        try:
            request_flag = list(request.keys())[0]
        except:
            pass
        # If the request was send by me, return
        # print(request.get(request_flag).get('sent_by').get('id'))
        if request.get(request_flag).get('sent_by').get('id') == self.id:
            return "SUCCESS"

        # print("\n::::::: REQUEST FLAG :::::::")

        if request_flag == flags.UPDATE_POINTS:
            # print('UPDATE_POINTS')
            points_to_update = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')

            # print("\n::::::: UPDATING POINTS :::::::")
            # print("-> BY:", sent_by.get('id'))

            showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=self.sendTo.get('id'),
                registred=list(self.topics),
                request_flag='UPDATE_POINTS',
                request_data=points_to_update,
                list_points=list(self.points)
            )

            for point in points_to_update:
                self.updatePoint(point, isNotifiable=False, )

            return "SUCCESS"

        if request_flag == flags.INSERT_POINTS:
            # print('INSERT_POINTS')
            points_to_add = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')

            # print("\n::::::: INSERTING POINTS :::::::")
            # print("-> BY:", sent_by.get('id'))

            self.insertReceivedPoints(points_to_add)

            showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=self.sendTo.get('id'),
                registred=list(self.topics),
                request_flag='INSERT_POINTS',
                request_data=points_to_add,
                list_points=list(self.points)
            )

            return "SUCCESS"

        if request_flag == flags.REMOVE_POINTS:
            # print('REMOVE_POINTS')
            points_to_remove = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')

            # print("\n::::::: REMOVING POINTS :::::::")
            # print("-> BY:", sent_by.get('id'))

            self.removePointByIds(points_to_remove, isNotifiable=False)

            showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=self.sendTo.get('id'),
                registred=list(self.topics),
                request_flag='REMOVE_POINTS',
                request_data=points_to_remove,
                list_points=list(self.points)
            )
            return "SUCCESS"

        if request_flag == flags.TOPIC_INSTANCES_RESPONSE:
            # print('TOPIC_INSTANCES_RESPONSE')
            received_topics = request.get(request_flag).get('topics')
            received_points = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')

            # Registre the sent_by topic
            self.registerTopic({sent_by.get('id'): sent_by})

            # Set the main topic to connect
            self.sendTo = sent_by
            # print("-> (CONNECTED TO)", self.sendTo.get('id'))

            # Remove known topics from request
            for key in list(self.topics):
                if received_topics.get(key):
                    del received_topics[key]

            # Register the new topics
            self.registerTopic(received_topics)

            # Save reveived points
            if received_points:
                self.insertReceivedPoints(received_points)
            
            # showLog(
            #     title=self.name.upper(),
            #     ip=self.ip,
            #     port=self.port,
            #     connect_to=self.sendTo.get('id'),
            #     registred=list(self.topics),
            #     request_flag='REMOVE_POINTS',
            #     request_data=received_points,
            #     list_points=list(self.points)
            # )
            print('que que ta rolando mano')
            print(self.sendTo, self.topics)
            return "SUCCESS"

    def registerTopic(self, topics=None):
        if topics:
            self.topics.update(topics)
            # print("::::::: REGISTRED TOPICS :::::::\n",
            #   json.dumps(self.topics, indent=2), "\n")


parser = argparse.ArgumentParser(description='Creating a Topic instance')

parser.add_argument('-tip', '--external_topic_ip',
                    help='IP of another Topic Instance')
parser.add_argument('-tp', '--external_topic_port',
                    help='PORT of nother Topic Instance')
parser.add_argument('-ip', '--ip', help='IP of this Topic Instance')
parser.add_argument('-p', '--port', help='PORT of this Topic Instance')
parser.add_argument('-n', '--name', help='This Topic Instance name')

args = vars(parser.parse_args())
# print (args)
subs = Subscriber(width=600, height=400, **args)

try:
    subscriber_server = Thread(target=subs.listen)
    subscriber_server.daemon = False
    subscriber_server.start()
    subs.run()
except Exception as e:
    raise
