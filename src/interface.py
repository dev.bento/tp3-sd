#!/usr/bin/python3

import random
import time
from prettytable import PrettyTable
import tableprint as tp
import numpy as np
import os
import json
from math import ceil


def showLog(title, ip, port, connect_to, registred, request_flag, request_data, list_points):
    os.system('clear')
    col_info = ['Listening at:', f'- ip: {ip}', f'- port: {port}', '-------------', 'Connect to:'] 

    if type(connect_to) == list:
        col_info += [f'- {cnn}' for cnn in connect_to]
    else:
        col_info += [connect_to]

    if registred:
        col_info += ['-------------', 'Registred:', ] + \
            ['- {}'.format(regs) for regs in registred]
    
    col_request = [request_flag, '-------------',]
    if type(request_data) == list:
        col_request += [f'- {v}' for v in request_data]
    elif request_flag == 'UPDATE_POINTS' or request_flag == 'INSERT_POINTS':
        if len(request_data) > 1:
            col_request += list(request_data)
        else:
            request_data = list(request_data.values())
            col_request += [f'- {k}: {v}' for k, v in request_data[0].items() if k != 'canvas_ids']
    else:
        col_request += [f'- {k}: {v}' for k, v in request_data.items()]
            # data = [value for key, value in request_data.items()]
        #register -> {'id': 'localhost:4444 (S)', 'ip': 'localhost', 'port': '4444', 'name': 'Subscriber 1', 'type': '(S)'}
        #insert -> {'1574472422': {'id': '1574472422', 'color': '#d4bee3', 'border': '#e86ea3', 'coord_x': 255, 'coord_y': 176, 'radius': 10, 'canvas_ids': {'localhost:4444 (S)': 1}}}
        # for item in data:
        # #  col_request += [f'- {key}:{value}' for key, value in item.items()]
        # print(request_data)
        # data = [value for _, value in request_data.items()]
        # for item in data:
        # #  col_request += [f'- {key}:{value}' for key, value in item.items()]
        #  col_request += [item]

    
    col_points = list_points

    nCols = 6
    nRows = max(ceil(len(col_points)/nCols), len(col_info), len(col_request))

    def reshapeRemanecent(list_row, nRows):
        remanecent = nRows-len(list_row)
        list_row += ['' for x in range(remanecent)]

    def reshapePoints(points, nRows):
        # rem_cols = nRows-(len(points) % nRows)
        i = 0
        temp = []
        while i+6 < len(points):
            temp.append(points[i:i+6])
            i += 6
        # i-=6
        temp.append(points[i:])
        temp += ['' for _ in range(nRows-len(temp))]
        return temp

    reshapeRemanecent(col_request, nRows)
    reshapeRemanecent(col_info, nRows)
    col_points = reshapePoints(col_points, nRows)

    x = PrettyTable()

    
    headers = [title, "REQUEST", "POINTS"]

    x.add_column(headers[0], col_info)
    x.add_column(headers[1], col_request)
    x.add_column(headers[2], col_points)

    x.align[title] = "l"
    x.align['REQUEST'] = "l"
    print(x)


# connect_to = ['localhost:2222 (T)', 'localhost:2223 (T)',
#               'localhost:2224 (S)', 'localhost:2225 (P)', ]
# registred = ['localhost:2222 (T)', 'localhost:2223 (T)']
# points = [91123123, 81231231231, 71123123, 6123123123, 51231231231, 41123123,
#               3123123123, 21231231231, 11123123, 1234323, 123123, 125122, 121312312]
# point = {
#     "id": '123123123',
#     "color": '#FFFFFF',
#     "border": "#AAAAAA",
#     "coord_x": 123,
#     "coord_y": 321,
#     "radius": 12,
#     "canvas_ids": 'self.canvas_ids'
# }

# remove_p = [123123, 123123, 546676]

# for n in range(100):
#     os.system('clear')  # on windows
#     points.append(random.randint(10000000, 1000000000))
#     showLog('localhost', '2222',connect_to, registred, 'REGISTER_AND_REQUEST_TOPIC_INSTANCES', remove_p, points)
#     time.sleep(.1)
