class Types():
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

    def __repr__(self):
        keys = sorted(self.__dict__)
        items = ("{}={!r}".format(k, self.__dict__[k]) for k in keys)
        return "{}({})".format(type(self).__name__, ", ".join(items))


flags = Types(
    RETURN_SUCCESS=True, 
    RETURN_FAILURE=False, 
    REGISTER_AND_REQUEST_TOPIC_INSTANCES = '0', 
    TOPIC_INSTANCES_RESPONSE = '1',
    INSERT_POINTS = '2',
    UPDATE_POINTS = '3',
    REMOVE_POINTS = '4',
    )