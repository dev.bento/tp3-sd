import sys
import argparse
import time
import socket
import json
import zlib
import selectors

from threading import Thread
from subprocess import check_output
from queue import Queue

from point import Point
from typefy import Types
from server import Server
from interface import showLog
from typefy import flags


BUFFER_SIZE = 100

# Threads:
# - Receive data from other topics, publishers and subscribers
# - Handle request instructions
# - Notify updates to other topics, publishers and subscribers


class Topic(Server):

    points = {}
    subscribers = {}
    publishers = {}
    topics = {}

    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

        self.type = '(T)'
        self.id = f"{self.ip}:{self.port} {self.type}"

        if not self.name:
            self.name = 'Topic'

        try:
            self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        except socket.error as msg:
            raise
            # print("Socket creation error: ", str(msg))
        if (self.external_topic_ip and self.external_topic_port):
            self.requestTopicInstances(
                self.external_topic_ip, self.external_topic_port, self.type)

        showLog(
                title=self.name.upper(),
                ip=self.ip,
                port=self.port,
                connect_to=list({**self.topics, **self.publishers, **self.subscribers}),
                registred=[],
                request_flag='',
                request_data={},
                list_points=list(self.points)
            )

        super(Topic, self).__init__(**kwargs)

    def handleRequest(self, request):
        try:
            request_flag = list(request.keys())[0]
        except:
            pass

        # print("\n::::::: REQUEST FLAG :::::::")
        sent_by = request.get(request_flag).get('sent_by')

        # if not sent_by or sent_by.get('id') == self.id: 
            # return 'SUCCESS'

        if request_flag == flags.UPDATE_POINTS:
            # print('UPDATE_POINTS')

            points_to_update = request.get(request_flag).get('points')

            # print("-> BY:", sent_by.get('id'))

            self.updatePoints(points_to_update)

            if request.get('delivered_by') != 'TOPIC':
                request['delivered_by'] = 'TOPIC'
                self.notifyTopics(self.topics, request)

            self.notifyPublishersAndSubscribers(request)
            showLog(
                title= self.name.upper(),
                ip=self.ip, 
                port=self.port, 
                connect_to=list({**self.topics, **self.publishers, **self.subscribers}),
                registred=[],
                request_flag= 'UPDATE_POINTS',
                request_data=points_to_update, 
                list_points = list(self.points)
            )
            return "SUCCESS"

        if request_flag == flags.INSERT_POINTS:
            # print('INSERT_POINTS')
            points_to_add = request.get(request_flag).get('points')

            # print("-> BY:", sent_by.get('name'))

            self.insertPoints(points_to_add)

            if request.get('delivered_by') != 'TOPIC':
                print(request)
                request['delivered_by'] = 'TOPIC'
                self.notifyTopics(self.topics, request)

            self.notifyPublishersAndSubscribers(request)
            
            showLog(
                title= self.name.upper(),
                ip=self.ip, 
                port=self.port, 
                connect_to=list({**self.topics, **self.publishers, **self.subscribers}),
                registred=[],
                request_flag= 'INSERT_POINTS',
                request_data=points_to_add, 
                list_points = list(self.points)
            )
            return "SUCCESS"

        if request_flag == flags.REMOVE_POINTS:
            # print('REMOVE_POINTS')
            points_to_remove = request.get(request_flag).get('points')

            # print("-> BY:", sent_by.get('name'))

            self.removePoints(points_to_remove)

            if request.get('delivered_by') != 'TOPIC':
                request['delivered_by'] = 'TOPIC'
                self.notifyTopics(self.topics, request)

            request.get(request_flag)['isNotifiable'] = False
            self.notifyPublishersAndSubscribers(request)

            showLog(
                title= self.name.upper(),
                ip=self.ip, 
                port=self.port, 
                connect_to=list({**self.topics, **self.publishers, **self.subscribers}),
                registred=[],
                request_flag= 'REMOVE_POINTS',
                request_data=points_to_remove, 
                list_points = list(self.points)
            )

            return "SUCCESS"

        if request_flag == flags.REGISTER_AND_REQUEST_TOPIC_INSTANCES:
            # print('REGISTER_AND_REQUEST_TOPIC_INSTANCES')
            request_by = request.get(request_flag).get('request_by')
            request_by__type = request.get(request_flag).get('type')

            self.registerInstance(request_by, request_by__type)

            message = json.dumps({
                flags.TOPIC_INSTANCES_RESPONSE: {
                    'sent_by': self.parseSelfToJson(),
                    'topics': self.topics,
                    'points': self.points,
                    'type': self.type
                }
            })

            showLog(
                title= self.name.upper(),
                ip=self.ip, 
                port=self.port, 
                connect_to=list({**self.topics, **self.publishers, **self.subscribers}),
                registred=[],
                request_flag= 'REGISTER_AND_REQUEST_TOPIC_INSTANCES',
                request_data=request_by, 
                list_points = list(self.points)
            )
            # print(self.topics)
            return message

        if request_flag == flags.TOPIC_INSTANCES_RESPONSE:


            received_topics = request.get(request_flag).get('topics')
            received_points = request.get(request_flag).get('points')
            sent_by = request.get(request_flag).get('sent_by')

            # Registre the sent_by topic
            self.registerInstance(sent_by, sent_by.get('type'))

            # Remove itself from topics list
            if received_topics.get(self.id):
                del received_topics[self.id]

            # Remove known topics from request
            for key in list(self.topics):
                if received_topics.get(key):
                    del received_topics[key]

            # Register the new topics
            if received_topics:
                self.registerInstance(received_topics, '(T)')

            # Save reveived points
            if received_points:
                self.insertPoints(received_points)

            # Register itself for each received topics
            for key, value in received_topics.items():
                self.requestTopicInstances(
                    value.get('ip'), value.get('port'), self.type)

            showLog(
                title= self.name.upper(),
                ip=self.ip, 
                port=self.port, 
                connect_to=list({**self.topics, **self.publishers, **self.subscribers}),
                registred=[],
                request_flag= 'TOPIC_INSTANCES_RESPONSE',
                request_data=received_points, 
                list_points = list(self.points)
            )
            print('TOPIC_INSTANCES_RESPONSE')
            # print(self.topics)

            return "SUCCESS"

    def registerInstance(self, instance, instance_type):

        inst = dict({instance.get('id'): instance})

        if instance_type == '(T)':
            self.topics.update(inst)
            # print("\n::::::: REGISTRED TOPICS :::::::")
            # print(json.dumps(list(self.topics), indent=2))

        if instance_type == '(S)':
            self.subscribers.update(inst)
            # print("\n::::::: REGISTRED SUBSCRIBERS :::::::")
            # print(json.dumps(list(self.subscribers), indent=2))

        if instance_type == '(P)':
            self.publishers.update(inst)
            # print("\n::::::: REGISTRED PUBLISHERS :::::::")
            # print(json.dumps(list(self.publishers), indent=2))

    def updatePoints(self, points):
        # print("\n::::::: UPDATING POINTS :::::::")
        print(points)
        for key, value in points.items():
            try:
                self.points.get(key).update(value)
                # print("-> id:", key, value)
            except Exception as e:
                raise

    def removePoints(self, points):
        # print("\n::::::: REMOVING POINTS :::::::")
        for point_id in points:
            try:
                if self.points.get(str(point_id)):
                    del self.points[str(point_id)]
                    # print("-> id:", json.dumps(point_id, indent=2))
            except Exception as e:
                raise

    def insertPoints(self, points):
        # print("\n::::::: INSERTING POINTS :::::::")
        for key, value in points.items():
            try:
                self.points[key] = value
                # print("-> id:", key)
            except Exception as e:
                raise

    def notifyPublishersAndSubscribers(self, message):

        # print(f"\n::::::: NOTIFYING :::::::")
        for key, subscriber in self.subscribers.items():
            notifier = Thread(target=self.notify, args=[subscriber, message])
            notifier.daemon = False
            notifier.start()

        for key, publisher in self.publishers.items():
            notifier = Thread(target=self.notify, args=[publisher, message])
            notifier.daemon = False
            notifier.start()


parser = argparse.ArgumentParser(description='Creating a Topic instance')

parser.add_argument('-tip', '--external_topic_ip',
                    help='IP of another Topic Instance')
parser.add_argument('-tp', '--external_topic_port',
                    help='PORT of nother Topic Instance')
parser.add_argument('-ip', '--ip', help='IP of this Topic Instance')
parser.add_argument('-p', '--port', help='PORT of this Topic Instance')
parser.add_argument('-n', '--name', help='This Topic Instance name')

args = vars(parser.parse_args())
topic = Topic(**args)
topic.listen()
